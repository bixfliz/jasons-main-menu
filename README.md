# Jason's Main Menu
jasons-main-menu

Command line Node.js configurable program launcher.

![Screenshot](https://gitlab.com/bixfliz/jasons-main-menu/raw/master/jmm.png "Screenshot")

Command to install: `npm i -g jasons-main-menu`

Run the program with the `command: jmm`

To configure edit the jmm-config.toml file in your home folder.  If you run the program and there is not current jmm-config.toml file then an example file will be created.

End the command with & if you want jmm to exit after launching the program (often needed for Windows OS).