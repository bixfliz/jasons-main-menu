#!/usr/bin/env node

// couldn't get these to work with typescript import.
const inquirerAutocompletePrompt = require('inquirer-autocomplete-prompt')
const inquirer = require('inquirer')
import { textSync } from 'figlet'
import { parse as tomlParse } from 'toml'
import { writeFileSync, existsSync, readFileSync } from 'fs'
import { random as lodashRandom } from 'lodash'
import { filter as fuzzyFilter, FilterResult } from 'fuzzy'
import { homedir } from 'os'
import { exec, execSync, ChildProcess } from 'child_process'
import chalk from 'chalk'
const strCongifFilename = 'jmm-config.toml'
const packageDetails = require('../package.json')
const strDefaultConfig: string = `# Jason' Main Menu config file. 
 
title = "Jason\'s     Menu"

[[item]]
title = "Cmder"
command = '"C:\\Program Files\\cmder\\vendor\\conemu-maximus5\\ConEmu64.exe"&'

[[item]]
title = "Command Prompt"
command = 'start "" cmd.exe /K cd /d c:\\&'

[[item]]
title = "SSH to my server"
command = 'ssh jason@special-server.com'

[[item]]
title = "Control Panel"
command = 'Control&'

[[item]]
title = "Windows Explorer"
command = 'explorer.exe&'

[[item]]
title = "Android Studio"
command = '"C:\\Program Files\\Android\\Android Studio\\bin\\studio64.exe"&'

[[item]]
title = "WinMerge"
command = '"C:\\Program Files\\WinMerge2011\\WinMergeU.exe"&'

[[item]]
title = "SQL Server 2014 Management Studio"
command = '"C:\\Program Files (x86)\\Microsoft SQL Server\\120\\Tools\\Binn\\ManagementStudio\\Ssms.exe"&'

[[item]]
title = "gVim"
command = '"C:\\Program Files (x86)\\Vim\\vim74\\gvim.exe"&'

[[item]]
title = "Notepad"
command = 'Notepad&'

[[item]]
title = "Edit Menu config notepad"
command = 'Notepad C:\\Users\\stracner\\jmm-config.toml&'

[[item]]
title = "Exit"
command = 'Exit'
`.replace('\n', '\r\n')
let strArrMenuOptions: { title: string; command: string }[]
let strArrMenuOptionsLabels: string[]
let oConfigFileContents: any
let strTitleText: string = "Jason's     Menu"

class JasonsMainMenu {
	public constructor() {
		try {
			let strConfigFilePath = homedir() + '/' + strCongifFilename
			if (!existsSync(strConfigFilePath)) {
				console.log(
					"Error: Can't find config file " + strConfigFilePath + '.  Creating an example one for you.',
				)
				writeFileSync(strConfigFilePath, strDefaultConfig)
			}

			try {
				oConfigFileContents = tomlParse(readFileSync(strConfigFilePath, 'utf-8'))
			} catch (oError) {
				console.error(
					'Parsing error on line ' + oError.line + ', column ' + oError.column + ': ' + oError.message,
				)
				process.exit()
			}

			if (oConfigFileContents.title) strTitleText = oConfigFileContents.title

			console.log(
				chalk.green(
					textSync(strTitleText, {
						font: 'Epic',
						horizontalLayout: 'default',
						verticalLayout: 'default',
					}),
				),
			)

			console.log(chalk.gray('      ver: ', packageDetails.version))

			strArrMenuOptions = oConfigFileContents.item
			strArrMenuOptionsLabels = strArrMenuOptions.map((eachMenuItem: any) => {
				return eachMenuItem.title
			})

			inquirer.registerPrompt('autocomplete', inquirerAutocompletePrompt)
			inquirer
				.prompt([
					{
						type: 'autocomplete',
						name: 'command',
						suggestOnly: false,
						message: 'Command?',
						source: (answers: string, strUserInput: string) => {
							strUserInput = strUserInput || ''
							return new Promise((resolve, reject) => {
								setTimeout(() => {
									let fuzzyResult: FilterResult<string>[] = fuzzyFilter(
										strUserInput,
										strArrMenuOptionsLabels,
									)

									resolve(
										fuzzyResult.map((el: FilterResult<string>) => {
											return el.original
										}),
									)
								}, lodashRandom(30, 500))
							})
						},
						pageSize: 20,
					},
				])
				.then((answers: { command: string }) => {
					let strCommand: string = ''

					for (let strMenuOption of strArrMenuOptions) {
						if (strMenuOption.title == answers.command) {
							strCommand = strMenuOption.command
							break
						}
					}

					if (strCommand.endsWith('&')) {
						strCommand = strCommand.substr(0, strCommand.length - 1)
						console.log('Running: ' + strCommand)
						exec(strCommand)
						setTimeout(() => {
							process.exit()
						}, 2000)
					} else {
						console.log('Running: ' + strCommand)
						execSync(strCommand, { stdio: [ 0, 1, 2 ] })
					}
				})
				.catch((error: string) => {
					throw error
				})
		} catch (e) {
			console.log(e)
		}
	}
}

new JasonsMainMenu()
